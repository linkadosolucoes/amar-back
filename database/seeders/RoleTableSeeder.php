<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seedsStoragePath = storage_path('seeds');
        $fileRolesPath = "{$seedsStoragePath}/roles.csv";

        $context = fopen($fileRolesPath, "r");
        while ($role = fgetcsv($context, 1000, ";")) {
            $roleExist = Role::where('object', $role[2])->count();
            if (!boolval($roleExist)) {
                Role::create([
                    'name' => $role[0],
                    'description' => $role[1],
                    'object' => $role[2],
                ]);
            }
        }

        fclose($context);
    }
}
