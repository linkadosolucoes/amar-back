<?php

namespace Database\Seeders;

use App\Models\Challenge;
use Illuminate\Database\Seeder;

class ChallengenTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = ["A", "B", "C", "D", "E"];

        $options = [
            $items[0] => "Opção A",
            $items[1] => "Opção B",
            $items[2] => "Opção C",
            $items[3] => "Opção D",
            $items[4] => "Opção E",
        ];

        for ($i = 1; $i <= 100; $i++) {
            Challenge::create([
                'title' => "Pergunta " . $i,
                'options' => json_encode($options),
                'correct_option' => $items[rand(0, 4)],
                'experience' => 50
            ]);
        }
    }
}
