<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\RoleUser;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Database\Seeder;

class RoleUserTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleUserManager = Role::where('object', 'user_management')->first();
        $roleStudent = Role::where('object', 'student')->first();

        $userSuperAdmin = User::where('email', 'superadmin@newcode.dev')->first();
        $userStudent = User::where('email', 'aluno@teste.com')->first();

        RoleUser::create([
            'role_id' => $roleUserManager->id,
            'user_id' => $userSuperAdmin->id
        ]);

        RoleUser::create([
            'role_id' => $roleStudent->id,
            'user_id' => $userStudent->id
        ]);
    }
}
