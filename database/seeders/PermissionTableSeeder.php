<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seedsStoragePath = storage_path('seeds');
        $filePermissionsPath = "{$seedsStoragePath}/permissions.csv";

        $context = fopen($filePermissionsPath, "r");
        while ($permission = fgetcsv($context, 1000, ";")) {
            $permissionExist = Permission::where('object', $permission[3])->count();
            if (!boolval($permissionExist)) {
                $role = Role::where('object', $permission[0])->first();

                Permission::create([
                    'role_id' => $role->id,
                    'name' => $permission[1],
                    'description' => $permission[2],
                    'object' => $permission[3],
                ]);
            }
        }

        fclose($context);
    }
}
