<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Verificação de E-mail</title>
</head>

<body>
    <p>Verificação de E-mail</p>

    <p>Olá, {{ $name }} </p>

    <p>Para validar seu e-mail, <a href="http://localhost:8080/verificar-email/{{$token}}">clique aqui</a>. </p>

    </br>
    </br>

    <em>Este e-mail tem validade de 15 minutos.</em>
</body>

</html>