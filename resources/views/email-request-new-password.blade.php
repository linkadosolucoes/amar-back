<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Solicitação - Nova Senha</title>
</head>

<body>
    <p>Solicitação de nova senha</p>

    <p>Olá, {{ $name }} </p>

    <p>Para criar uma nova senha, <a href="http://localhost:8080/criar-nova-senha/{{$token}}">clique aqui</a>. </p>
</body>

</html>