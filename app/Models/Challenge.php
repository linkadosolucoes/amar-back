<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Challenge extends Model
{
    protected $fillable = [
        'title',
        'options',
        'correct_option',
        'experience'
    ];

    protected $hidden = [
        'correct_option',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function response()
    {
        return $this->hasOne(\App\Models\Response::class);
    }
}
