<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use SoftDeletes, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'email_verified_at',
        'password',
        'blocked',
        'disabled'
    ];

    protected $hidden = [
        'password',
        'deleted_at'
    ];

    protected $casts = [
        'blocked' => 'boolean',
        'disabled' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
        'email_verified_at' => 'datetime',
    ];

    public function getEmailVerifiedAt()
    {
        return $this->email_verified_at;
    }

    public function isBlocked()
    {
        return $this->blocked;
    }

    public function isDisabled()
    {
        return $this->disabled;
    }

    public function roles()
    {
        return $this->belongsToMany(\App\Models\Role::class);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
