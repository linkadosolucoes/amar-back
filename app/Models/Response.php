<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $fillable = [
        'challenge_id',
        'user_id',
        'response_option',
        'correct'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'correct' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];
}
