<?php

namespace App\Project;

class Setting
{
    public static function getSettingMultipleLogin()
    {
        return getenv("SETTING_MULTIPLE_LOGIN") === "true" ? true : false;
    }

    public static function getSettingEmailVerify()
    {
        return getenv("SETTING_EMAIL_VERIFY") === "true" ? true : false;
    }
}
