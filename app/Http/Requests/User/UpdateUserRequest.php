<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['string'],
            'email' => ['string', 'email'],
            'password' => ['string', 'min:8'],
            'disabled' => ['boolean'],
            'roles' => ['array', 'min:1'],
            'blocked' => ['boolean']
        ];
    }
}
