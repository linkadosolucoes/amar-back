<?php

namespace App\Http\Requests\Response;

use Illuminate\Foundation\Http\FormRequest;

class PostResponseRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'challenge_id' => ['required', 'numeric'],
            'response_option' => ['required', 'string', 'min:1']
        ];
    }
}
