<?php

namespace App\Http\Controllers\V1;

use App\Models\Response;
use App\Models\Challenge;
use App\Project\ResponseApi;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\Response\PostResponseRequest;

class ResponseController extends Controller
{
    public function store(PostResponseRequest $request)
    {
        try {
            $challenge = Challenge::find($request->challenge_id);
            if (is_null($challenge)) {
                return ResponseApi::error(null, "Desafio não localizado.", 404);
            }

            $hasResponse = Response::where('challenge_id', $challenge->id)->where('user_id', Auth::user()->id)->count();
            if ($hasResponse >= 1) {
                return ResponseApi::error(null, "Desafio já respondido.", 400);
            }

            $isCorrect = $challenge->correct_option === $request->response_option ? true : false;

            $data = [
                'challenge_id' => $challenge->id,
                'user_id' => Auth::user()->id,
                'response_option' => $request->response_option,
                'correct' => $isCorrect,
            ];

            $response =  Response::create($data);

            return ResponseApi::success($response);
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }
}
