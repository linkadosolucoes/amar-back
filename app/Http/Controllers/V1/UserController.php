<?php

namespace App\Http\Controllers\V1;

use Carbon\Carbon;
use App\Models\User;
use App\Project\Setting;
use App\Models\RoleUser;
use App\Models\EmailVerify;
use Illuminate\Support\Str;
use App\Project\ResponseApi;
use App\Mail\MailEmailVerify;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\User\PostUserRequest;
use App\Http\Requests\User\UpdateUserRequest;

class UserController extends Controller
{
    public function index()
    {
        try {
            return ResponseApi::success(User::orderBy('name', 'asc')->paginate(10));
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }

    public function show($id)
    {
        try {
            $user = User::with(["roles"])->find($id);
            if (is_null($user)) {
                return ResponseApi::error(null, "Usuário não localizado.", 404);
            }

            $listRoles = [];
            foreach ($user->roles as $role) {
                array_push($listRoles, $role->id);
            }

            $user = $user->toArray();
            $user['roles'] = $listRoles;

            return ResponseApi::success($user);
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }

    public function store(PostUserRequest $request)
    {
        try {
            $dataNewUser = $request->all();
            if ($request->has('password')) {
                $dataNewUser['password'] = bcrypt($dataNewUser['password']);
            }

            $emailRegistered = User::where('email', $request->email)->count();
            if (boolval($emailRegistered)) {
                return ResponseApi::error(null, "Email já cadastrado.");
            }

            $user = DB::transaction(function () use ($dataNewUser) {
                $user = User::create($dataNewUser);

                foreach ($dataNewUser['roles'] as $roleId) {
                    RoleUser::create([
                        'role_id' => $roleId,
                        'user_id' => $user->id
                    ]);
                }

                if (Setting::getSettingEmailVerify()) {
                    $emailVerify = EmailVerify::create([
                        'user_id' => $user->id,
                        'token' => Str::random(200),
                        'revoged' => false,
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                    Mail::to($user->email)->send(new MailEmailVerify($user, $emailVerify));
                }

                return $user;
            });

            return ResponseApi::success($user);
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }

    public function update(UpdateUserRequest $request, $id)
    {
        try {
            $user = User::find($id);
            if (is_null($user)) {
                return ResponseApi::error(null, "Usuário não localizado.", 404);
            }

            $dataUser = $request->all();
            if ($request->has('password')) {
                $dataUser['password'] = bcrypt($dataUser['password']);
            }

            DB::transaction(function () use ($user, $dataUser) {
                $user->update($dataUser);

                if (count($dataUser['roles']) >= 1) {
                    RoleUser::where('user_id', $user->id)->delete();

                    foreach ($dataUser['roles'] as $roleId) {
                        RoleUser::create([
                            'role_id' => $roleId,
                            'user_id' => $user->id
                        ]);
                    }
                }
            });

            return ResponseApi::success();
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            $user = User::find($id);
            if (is_null($user)) {
                return ResponseApi::error(null, "Usuário não localizado.", 404);
            }

            $user->delete();

            return ResponseApi::success();
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }
}
