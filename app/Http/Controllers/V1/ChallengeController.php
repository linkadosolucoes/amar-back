<?php

namespace App\Http\Controllers\V1;

use App\Models\Challenge;
use App\Project\ResponseApi;
use App\Http\Controllers\Controller;

class ChallengeController extends Controller
{
    public function index()
    {
        try {
            return ResponseApi::success(Challenge::with(['response'])->orderBy('id', 'asc')->paginate(10));
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }

    public function next()
    {
        try {
            $challenge = Challenge::doesntHave('response')->orderBy('id', 'asc')->first();
            if (is_null($challenge)) {
                return ResponseApi::error(null, "Desafio não disponível.", 404);
            }

            return ResponseApi::success($challenge);
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }

    public function show($id)
    {
        try {
            $challenge = Challenge::find($id);
            if (is_null($challenge)) {
                return ResponseApi::error(null, "Desafio não localizado.", 404);
            }

            return ResponseApi::success($challenge);
        } catch (\Throwable $th) {
            return ResponseApi::errorServer($th->getMessage());
        }
    }
}
